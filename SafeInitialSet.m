%%%%%%%%%%%%
% This code is written by:
% Birgit van Huijgevoort
% Eindhoven University of Technology
% b.c.v.huijgevoort@tue.nl
%
% Last update: 09/04/2019
% License: BSD-3

tic;

Ts = 20;
umax = 7;
umin = 0; 
n = 3;
%% Discrete system from paper
% x[k+1] = Ax[k]+Bu[k]+Gv[k]

A = [0.8192, 0.03412, 0.01265; 0.01646, 0.9822, 0.0001; 0.0009, 0.00002, 0.9989];
B = [0.01883; 0.0002; 0.00001];

G = [1;1;1];

%% Truncate noise
% Assumes diagonal and constant standard deviation matrix
mu = 0;
sigma_squared = 1e-3;
delta_totaal = 1e-4;
delta = delta_totaal/n;

sigma = sqrt(sigma_squared);
w = norminv([(delta)/2, 1-(delta)/2],mu,sigma);
wmin = w(1);
wmax = w(2);

%% Specify bounds on noise and input (u=sigma)
w = Polyhedron('lb', wmin, 'ub', wmax);  % Disturbance bounds
u = Polyhedron('lb', umin, 'ub', umax);  % Input bounds  

W = w*w*w;

sys = ss(A,B,eye(3),0,Ts);
model = LTISystem(sys);

%% Specify set of initial conditions and time interval
S0 = Polyhedron('lb', [1; 0; 0], 'ub', [6; 10; 10]);
N = 10;

SN = cell(N,1);
SN{1} = S0;
for i = 1:N
    [SN{i+1},S]=model.reachableSet('X',SN{i}-W,'U',u,'N',1, 'direction', 'backward');
    SN{i+1} = SN{i+1} & S0;
end

toc

%% Plot the results
figure;
linecolors = gray(2*N);
hold on
for i = 1:N
    SN{i}.plot('Color',linecolors(N+i,:),'FaceAlpha',0.5);
end
%x0.plot()
hold off  

figure;
plot(SN{end})
xlabel('x_1')
ylabel('x_2')
zlabel('x_3')
xlim([1 6])
ylim([0 10])
zlim([0 10])